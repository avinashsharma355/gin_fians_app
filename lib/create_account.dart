import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ginfiansapp/compexity.dart';
import 'package:ginfiansapp/main.dart';
import 'package:ginfiansapp/personal_info.dart';
import 'package:ginfiansapp/util.dart';

class AccountPage extends StatefulWidget {
  AccountPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  String mComplexity = "Very Weak";
  final TextEditingController _passwordController = TextEditingController();
  String errorTextPassWord = "";
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _passwordController.addListener(() {
      print("value: ${_passwordController.text}");
      //use setState to rebuild the widget
      setState(() {

      });
    });
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppUtil.bgColor,
        title: Text(
          "Create Account",
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 15,
              fontStyle: FontStyle.normal,
              color: Colors.white),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
          width: double.infinity,
          height: double.infinity,
          color: AppUtil.bgColor,
          alignment: Alignment.topCenter,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:
                      EdgeInsets.only(left: 10, bottom: 20, top: 5, right: 10),
                  child: Row(
                    children: buildDots(2), //show app step
                  ),
                ),
                _textView("Create Password", 16),
                _textView("Password will be used to loin to account", 12),
                Padding(
                    padding: EdgeInsets.only(left: 15, top: 30, right: 15),
                    child: _entryField("Create Password", _passwordController,
                        isPassword: true)),
                Padding(
                    padding: EdgeInsets.only(left: 15, top: 10, right: 15),
                    child: Row(
                      children: <Widget>[
                        Text(
                          "Complexity : ",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w900),
                        ),
                        Text(
                          mComplexity,
                          style: TextStyle(
                              color: Color(0xFFBF9B3E),
                              fontSize: 12,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w900),
                        ),
                      ],
                    )),
        Padding(
            padding: EdgeInsets.only(left: 0, top: 20, right: 0),
            child:     Row(
                  children: _compelxityLevel(0),
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                )),


                Padding(
                    padding: EdgeInsets.only(left: 15, top: 30, right: 15),
                    child: _nextButton("Next")),
              ])),
    );
  }
  final _comText = [
    Complexity("Lowersase", "a", false),
    Complexity("Uppercase", "A", false),
    Complexity("Number", "123", false),
    Complexity("Characters", "9+", false)
  ];
  List<Widget> _compelxityLevel(int current_step) {

    var wids = <Widget>[];
    _comText.asMap().forEach((i, text) {


      wids.add(Column(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: text.isverify
                  ? Image.asset('images/checked.png')
                  : Text(
                      text.title,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    )),
          Text(
            text.name,
            style: TextStyle(
                color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold),
          )
        ],
      ));
    });

    return wids;
  }

  Widget _textView(String txt, double font) {
    return Padding(
      padding: EdgeInsets.only(left: 15, top: 8, right: 15),
      child: Text(
        txt,
        style: TextStyle(
            color: Colors.white,
            fontSize: font,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w900),
      ),
    );
  }

  bool _isHidden = true;
  var hintText;

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  Widget _entryField(String title, TextEditingController control,
      {bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Material(
            elevation: 20,
            shadowColor: Colors.black,
            borderRadius: BorderRadius.all(Radius.circular(12)),
            child: TextField(
                controller: control,
                obscureText: _isHidden,
                onChanged: (value) {
                  print("_passwordController value: ${_passwordController.text}");
                  print("value: ${value}");

                  int countAllTrue=0;
                  //use setState to rebuild the widget
                  if(value.contains(RegExp(r'[A-Z]'))){
                    _comText[1].isverify=true;
                    print("value: Uppercase ture");
                    countAllTrue++;
                  }else{
                    _comText[1].isverify=false;

                  }
                  if(value.contains(RegExp(r'[a-z]'))){
                    print("value: lowercase ture");
                    _comText[0].isverify=true;
                    countAllTrue++;
                  }else{
                    _comText[0].isverify=false;
                  }

                  if(value.contains(RegExp(r'[0-9]'))){
                    print("value: num ture");
                    _comText[2].isverify=true;
                    countAllTrue++;
                  }else{
                    _comText[2].isverify=false;
                  }
                    if(value.length>9){
                    print("value: char ture");
                    _comText[3].isverify=true;
                    countAllTrue++;
                  }else{
                      _comText[3].isverify=false;
                    }

if(countAllTrue==3){
  mComplexity="Normal";
}else if(countAllTrue==4){
  mComplexity="Very Strong";

}else{
  mComplexity="Very Weak";
}

                  setState(() {

                  });
                },
                decoration: InputDecoration(
                    hintText: title,
                    suffixIcon: IconButton(
                      onPressed: _toggleVisibility,
                      icon: Image.asset(_isHidden
                          ? 'images/visibility.png'
                          : 'images/hide.png'),
                      color: Colors.amberAccent,
                      iconSize: 10,
                    ),
                    border: UnderlineInputBorder(
                        borderRadius: BorderRadius.circular(12.0)),
                    fillColor: Color(0xffF5F5F5),
                    filled: true)),
          ),
        ],
      ),
    );
  }




  Widget _nextButton(String title) {
    return InkWell(
      onTap: () {


        if(mComplexity=="Very Strong"){

          Navigator.push(context, MaterialPageRoute(builder: (context) => PersonalInfoPage()));
        }else{

          _dialog();

        }

        //
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [AppUtil.buttonColor, AppUtil.buttonColor])),
        child: Text(
          title,
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  Future<bool> _dialog() {
    return showDialog(
      context: context,
      builder: (context) =>  AlertDialog(
        title: new Text('Password'),
        content: new Text("Please set a Strong Password"),
        actions: <Widget>[
          GestureDetector(
            onTap: () => Navigator.of(context).pop(true),
            child: Padding(padding: EdgeInsets.all(15),child:Text("OK") ,) ,
          ),
        ],
      ),
    ) ??
        false;
  }
}

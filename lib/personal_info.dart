import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ginfiansapp/compexity.dart';
import 'package:ginfiansapp/home.dart';
import 'package:ginfiansapp/main.dart';
import 'package:ginfiansapp/util.dart';

class PersonalInfoPage extends StatefulWidget {
  PersonalInfoPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PersonalInfoPageState createState() => _PersonalInfoPageState();
}

class _PersonalInfoPageState extends State<PersonalInfoPage> {
  // final TextEditingController _passwordController = TextEditingController();
  String errorTextPassWord = "";

  String dropdownGoalValue = null;
  String dropdownIncomeValue = null;
  String dropdownexpenseValue = null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppUtil.bgColor,
        title: Text(
          "Create Account",
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 15,
              fontStyle: FontStyle.normal,
              color: Colors.white),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
          width: double.infinity,
          height: double.infinity,
          color: AppUtil.bgColor,
          alignment: Alignment.topCenter,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:
                      EdgeInsets.only(left: 10, bottom: 20, top: 5, right: 10),
                  child: Row(
                    children: buildDots(3), //show app step
                  ),
                ),
                _textView("Personal Information", 16),
                _textView(
                    "Please fill in the information beow and your goal for digital saving.",
                    12),
                Padding(
                  padding: EdgeInsets.only(left: 15, top: 10, right: 15),
                  child: _dropDownTxt(_goalList,dropdownGoalValue,"goal"),
                ), Padding(
                  padding: EdgeInsets.only(left: 15, top: 10, right: 15),
                  child: _dropDownTxt(_incomeList,dropdownIncomeValue,"income"),
                ), Padding(
                  padding: EdgeInsets.only(left: 15, top: 10, right: 15),
                  child: _dropDownTxt(_expensesList,dropdownexpenseValue,"expense"),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 15, top: 10, right: 15),
                    child: _nextButton("Next")),
              ])),
    );
  }

  Widget _textView(String txt, double font) {
    return Padding(
      padding: EdgeInsets.only(left: 15, top: 8, right: 15),
      child: Text(
        txt,
        style: TextStyle(
            color: Colors.white,
            fontSize: font,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w900),
      ),
    );
  }

  final _goalList = ["Child Education", "Saving", "Home", "Car"];
  final _incomeList = ["\$ >50000", "\$ >150000", "\$ >250000", "\$ >350000"];
  final _expensesList =  ["\$ >5000", "\$ >10000", "\$ >20000", "\$ >50000"];

  Widget _dropDownTxt(List<String> list,String dropdownValue,String type) {

    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Material(
            elevation: 20,
            shadowColor: Colors.black,
            textStyle: TextStyle(color: Colors.black87, fontSize: 15),
            borderRadius: BorderRadius.all(Radius.circular(12)),
            child: DropdownButton<String>(

              isExpanded: true,
              hint: dropdownValue == null
                  ?Padding(padding: EdgeInsets.only(left: 5,right: 5),
                 child: Text("-Choose Option-", style: TextStyle(color: Colors.black87,fontSize: 15)),)
                  :Padding(padding: EdgeInsets.only(left: 5,right: 5),
                child: Text(dropdownValue, style: TextStyle(color: Colors.black87,fontSize: 15)),) ,
              focusColor: Colors.black87,
              icon: Icon(
                Icons.arrow_drop_down,
                color: Colors.grey,
              ),
              iconSize: 24,
              underline: null,
              elevation: 16,
              style: TextStyle(color: Colors.black87, fontSize: 15),
              dropdownColor: Colors.blue,
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue = newValue;
                  if(type=="goal"){
                    dropdownGoalValue = newValue;
                  }else if(type=="income"){
                    dropdownIncomeValue = newValue;

                  }else{
                    dropdownexpenseValue = newValue;

                  }

                });
              },
              items: list.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  onTap: () {},
                  child: _textView(value, 12),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _nextButton(String title) {
    return InkWell(
      onTap: () {
        if(dropdownGoalValue==null){

          _dialog("Please select your goal");

        }else if(dropdownIncomeValue==null){
          _dialog("Please select your monthly income");

        }else if(dropdownexpenseValue==null){

          _dialog("Please select your monthly expense");
        }else{

          Navigator.push(context, MaterialPageRoute(builder: (context) => HeartAnimationWidget()));
        }
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [AppUtil.buttonColor, AppUtil.buttonColor])),
        child: Text(
          title,
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  Future<bool> _dialog(String msg) {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: new Text('Error!!'),
            content: new Text(msg),
            actions: <Widget>[
              GestureDetector(
                onTap: () => Navigator.of(context).pop(true),
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Text("OK"),
                ),
              ),
            ],
          ),
        ) ??
        false;
  }
}

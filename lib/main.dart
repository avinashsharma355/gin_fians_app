import 'package:flutter/material.dart';
import 'package:ginfiansapp/util.dart';

import 'create_account.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: ''),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home  page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

 //int current_step = 0;

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _emailController =
  TextEditingController();
  String errorTextEmail="";
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body:   Container(
          color: Color(AppUtil.colorapp),
          child: CustomPaint(
            painter: CurvePainter(), //BAckground paint
            child: Container(
              width: double.infinity,
              alignment: Alignment.topLeft,
              child: SafeArea(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10, bottom: 20, top: 30, right: 10),
                    child: Row(
                      children: buildDots(1), //show app step
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 15, top: 50, right: 15),
                    child: Text(
                      "Welcome to\nGIN Finans",
                      style: TextStyle(
                          color: Color(AppUtil.colorText),
                          fontSize: 35,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w900),
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 15, top: 30, right: 15),
                    child: Text(
                      "Wecome to The Bank of The Future.\nManage and track your accounts on\nthe go.",
                      style: TextStyle(
                          color: Color(AppUtil.colorText),
                          fontSize: 16,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w900),
                    ),
                  ),
                  //Wecome to The Bank of The Future.Manage and track your accounts on the go.
                  Padding(
                      padding: EdgeInsets.only(left: 15, top: 30, right: 15),
                      child: _entryField("Email",_emailController)),
                  Padding(
                      padding: EdgeInsets.only(left: 15, top: 30, right: 15),
                      child: _nextButton("Next")),
                ],
              )),
            ),
          )),
    );
  }



  Widget _entryField(String title,TextEditingController control, {bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Material(
            elevation: 20,
            shadowColor: Colors.black,
            borderRadius:BorderRadius.all(Radius.circular(12)),
            child: TextField(
              controller:control ,
                obscureText: isPassword,
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email),

                    hintText: title,
                    border: UnderlineInputBorder(
                        borderRadius: BorderRadius.circular(12.0)),
                    fillColor: Color(0xffF5F5F5),
                    filled: true)),
          ),
        ],
      ),
    );
  }

  Widget _nextButton(String title) {
    return InkWell(
      onTap: () {

       var str= _emailController.text ;
        if(AppUtil.isEmail(str)){
          errorTextEmail="";
          //current_step=1;
          Navigator.push(context, MaterialPageRoute(builder: (context) => AccountPage()));
        }else{
          errorTextEmail="Enter valid email";
          _dialog();

        }

       //
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [AppUtil.buttonColor, AppUtil.buttonColor])),
        child: Text(
          title,
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }
  Future<bool> _dialog() {
    return showDialog(
      context: context,
      builder: (context) =>  AlertDialog(
        title: new Text('Email'),
        content: new Text(errorTextEmail),
        actions: <Widget>[
           GestureDetector(
            onTap: () => Navigator.of(context).pop(true),
            child: Padding(padding: EdgeInsets.all(15),child:Text("OK") ,) ,
          ),
        ],
      ),
    ) ??
        false;
  }


}
List<Widget> buildDots(int current_step) {
  final _stepsText = ["1", "2", "3", "4"];
  var wids = <Widget>[];
  _stepsText.asMap().forEach((i, text) {
    var circleColor = (current_step > i + 1)
        ? AppUtil.activeColor
        : AppUtil.inactiveColor;

    var lineColor =
    current_step > i + 1 ? AppUtil.activeColor : AppUtil.inactiveColor;

    wids.add(Stack(
      alignment: Alignment.center,
      children: <Widget>[
        CircleAvatar(
          radius: 20,
          backgroundColor: circleColor,
        ),
        Text(
          text,
          style: TextStyle(
              color: Colors.black87,
              fontSize: 15,
              fontWeight: FontWeight.bold),
        )
      ],
    )); /*daraw circle with steps  1---2---3---4*/

    //add a line separator
    //0-------0--------0
    if (i != _stepsText.length - 1) {
      wids.add(Expanded(
          child: Container(
            height: 4,
            color: lineColor,
          ) /*draw line*/
      ));
    }
  });

  return wids;
}



 import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppUtil {
  static const int colorapp =0xFF3F7BEA;//3F7BEB
  static const int colorText =0xFF0A0A0A;
  static const  Color activeColor = Color(0xFF76A459);

  static const Color inactiveColor = Color(0xFFE9EDEE) ;
  static const Color buttonColor = Color(0xFF7DA0E4) ;
  static const Color bgColor = Color(0xFF407CEE) ;

  static  bool isEmail(String email) {

    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(email);
  }

}

 class CurvePainter extends CustomPainter {
   @override
   void paint(Canvas canvas, Size size) {
     var paint = Paint();
     paint.color = Color(0xFFF8F8F8);
     paint.style = PaintingStyle.fill;

     var path = Path();

     path.moveTo(0, size.height * 0.2);
     path.quadraticBezierTo(size.width * 0.15, size.height * 0.10,
         size.width , size.height * 0.28);
     path.relativeQuadraticBezierTo(0, 3, 30, -5);
     //path.quadraticBezierTo(size.width * 0.75, size.height * 0.9584, size.width * 1.0, size.height * 0.9167);
     path.lineTo(size.width, size.height);
     path.lineTo(0, size.height);

     canvas.drawPath(path, paint);
   }

   @override
   bool shouldRepaint(CustomPainter oldDelegate) {
     return true;
   }
 }